! ========================================================= 
! sindarin.lexc
! @content: sindarin lexicon file
! @author: Thomas Zink, Andreas Engl
! =========================================================
! Usage: read lexc < [ThisFile]
! =========================================================
Multichar_Symbols

! TAGS
+Det +Indef +Def +Gen +Sg +Pl +Nsg +Npl +Ngen +Noun +Norm +Ath

! DETERMINATION AND CASE
@P.ART.INDEF@ @P.CASE.GEN@ @P.NUM.SG@ @P.NUM.PL@
@R.ART.INDEF@ @R.CASE.GEN@ @R.NUM.SG@ @R.NUM.PL@
@D.ART.INDEF@ @D.CASE@

! MUTATIONS
@P.MUT.SOFT@ @P.MUT.NAS@ @P.MUT.MIX@
@R.MUT.SOFT@ @R.MUT.NAS@ @R.MUT.MIX@

! SINGULAR SUFFIXES
@P.SUFF.OD@ @P.SUFF.OG@ @P.SUFF.IG@
@R.SUFF.OD@ @R.SUFF.OG@ @R.SUFF.IG@

! NORMALIZATION, PLURAL NOUN, DETERMINER MUTATION
@P.NORM.T@ @R.NORM.T@
@P.PN.T@ @R.PN.T@ @D.PN.T@
@P.I.T@ @R.I.T@ @D.I.T@
@P.C.T@ @R.C.T@ @D.C.T@

! NOUN START
@P.N.S@
! =========================================================
! LEXICON
! =========================================================
LEXICON Root
	Article;
	Nstart;
! =========================================================
LEXICON Article
i-@P.NUM.SG@				MarkA;
ir@P.NUM.SG@@P.I.T@			MarkA;
in@P.NUM.PL@				MarkA;
i@P.NUM.PL@@P.C.T@			MarkA;
en@P.CASE.GEN@				MarkA;
! =========================================================
LEXICON MarkA
<"+Det":0 "@D.I.T@" "@D.C.T@">		MarkCaseNum;
<"+iDet":0 "@R.I.T@">			MarkCaseNum;
<"+cDet":0 "@R.C.T@">			MarkCaseNum;
! =========================================================
LEXICON Nstart
<"@P.ART.INDEF@" "@P.NUM.SG@">		Nouns; 
<"@P.ART.INDEF@" "@P.NUM.PL@">  	Nouns;
<"@P.NUM.SG@" "@P.MUT.SOFT@">		Nouns;
<"@P.NUM.PL@" "@P.MUT.NAS@">		Nouns;
<"@P.CASE.GEN@" "@P.MUT.MIX@">		Nouns;
<"@P.ART.INDEF@" "@P.PN.T@">		Plnouns;
! =========================================================
LEXICON Nouns
<"@P.N.S@">				regN;
<"@P.I.T@" "@P.N.S@">			iN;
<"@P.C.T@" "@P.N.S@">			cN;
!<"@P.NORM.T@" "@P.N.S@">		normN;
<"@P.I.T@" "@P.NORM.T@" "@P.N.S@">	inormN;
<"@P.C.T@" "@P.NORM.T@" "@P.N.S@">	cnormN;
! =========================================================
LEXICON regN
acharn			Nend;	! vengeance
amon		      	Nend;	! hill
aphadon		      	Nend;	! follower
aran		      	Nend;	! king
arnad			Nend;	! kingdom
adan		      	Nend;	! man
adar		      	Nend;	! father
adanadar	      	Nend;	! father of the edain
alph		      	Nend;	! swan
aegas			Nend;	! mountain peak
edhel		      	Nend;	! elf
echad         	  	Nend;	! camp
eilian        	  	Nend;	! rainbow
emil          	  	Nend;	! mother
enedh         	  	Nend;	! middle
eneth         	  	Nend;	! name
eru           	  	Nend;	! desert
orn           		Nend;	! tree
odhril        		Nend;	! mother
orch          		Nend;	! orc
onod		      	Nend;	! ent
urug          		Nend;	! monster
ylf           		Nend;	! drinking vessel
! =========================================================
LEXICON iN
idhil      	Nend;	! moon
^idh            	Nend; 	! rest
idhor         	      	Nend; 	! thoughfulness
ithron       	      	Nend; 	! wizard
ivor          	      	Nend; 	! crystal
iaun          		Nend;	! sanctuary
i^ath         		Nend;	! fence
iuith         		Nend;	! use
! =========================================================
LEXICON cN
barad		      	Nend;	! tower
blabed		      	Nend;	! flapping
br^og         	  	Nend; 	! bear
brethil		      	Nend;	! birch
bereth			Nend;
cant		      	Nend;	! shape
calardan	      	Nend;	! lampwright
claur		      	Nend;	! splendor
craban		      	Nend;	! crow
d^ur		     	Nend;	! darkness
d^ol		      	Nend;	! hill
duin		      	Nend;	! river
draug		      	Nend;	! wolf
fang		      	Nend;	! beard
fl^ad         	  	Nend;	! skin
fair		      	Nend;	! mortal
fend         	  	Nend;	! door
fern          	  	Nend;	! dead
galadh		      	Nend;	! tree
gerth		      	Nend;	! rune
gl^an		      	Nend;	! border
gl^i          	  	Nend;	! honey
graug		      	Nend;	! demon
grond         	  	Nend;	! club
g^il          	  	Nend;	! spark
gilith        	  	Nend;	! starlight
gw^ath		      	Nend;	! shadow
gwathel			Nend; 	! sister
gwael		      	Nend; 	! gull
hammad        	    	Nend; 	! clothing
harven        	    	Nend; 	! south
h^al          	    	Nend; 	! fish
h^ith		      	Nend;	! mist
h^un          	    	Nend; 	! heart
hwest		      	Nend;	! breeze
hw^in         	     	Nend; 	! faintness
lam           		Nend;	! tongue, language
lach          		Nend;	! flame
lunt		      	Nend;	! boat
l^in		      	Nend;	! pool
l^um          		Nend;	! shade
l^yg          		Nend;	! snake
loth          		Nend;	! flower
las		        Nend;	! leaf
lh^e          		Nend;	! filament
lh^iw         		Nend;	! sickness
lhos          		Nend;	! whisper
lh^ug		      	Nend;	! dragon
megil		      	Nend;	! sword
mellon        		Nend;	! friend
miniel		    	Nend;	! first elf
mirion		    	Nend;	! silmaril, great jewel
m^ir		      	Nend;	! jewel
morben		    	Nend;	! dark elf
m^yl		      	Nend;	! gull
naug		      	Nend;	! dwarf
nath          		Nend;	! web
n^en          		Nend;	! water
p^an          		Nend;	! plank
parth		      	Nend;	! field
prestad		    	Nend;	! disturbance
p^od		      	Nend;	! animal's foot
ras		        Nend;	! horn
raud          		Nend;	! metal
roch          		Nend;	! horse
roval         		Nend;	! pinion, wing
rhavan		    	Nend;	! wild man
salph         		Nend;	! soup
sereg         		Nend;	! blood
s^ir          		Nend;	! river
t^al          		Nend;	! foot
taur		      	Nend;	! forest
taith         		Nend;	! mark
trann		      	Nend;	! shire
t^ew		      	Nend;	! letter
tulus		      	Nend;	! poplar tree
trenarn       		Nend;	! tale
thand		      	Nend;	! shield
thond         		Nend;	! root
! =========================================================
!LEXICON normN
! =========================================================
LEXICON inormN
idh			Nend;
iath			Nend;
! =========================================================
LEXICON cnormN
brog			Nend;
dur  			Nend;
dol			Nend;
flad			Nend;
glan			Nend;
gli			Nend;
gil			Nend;
gwath			Nend;
hal			Nend;
hith			Nend;
hun			Nend;
hwin			Nend;
lin			Nend;
lum			Nend;
lyg			Nend;
lhe			Nend;
lhiw			Nend;
lhug			Nend;
mir			Nend;
myl			Nend;
nen			Nend;
pan			Nend;
pod			Nend;
sir			Nend;
tal			Nend;
tew			Nend;
! =========================================================
! some nouns are basic form plural
! they have optional singular endings
LEXICON Plnouns
<"@P.SUFF.OG@">		Og;
<"@P.SUFF.IG@">		Ig;
<"@P.SUFF.OD@">		Od;
! =========================================================
LEXICON Og
glam   			Nend;	! orcs
! =========================================================
LEXICON Ig
gwan^un			Nend;	! twins
gwanun@P.NORM.T@	Nend;
lhaw   			Nend;	! ears
n^el   			Nend;	! teeth
nel@P.NORM.T@		Nend;
! =========================================================
LEXICON Od
filig  			Nend;	! small birds
! =========================================================
LEXICON Nend
<"@D.NORM.T@">				MarkDef;
<"+Norm":0 "@R.NORM.T@">		MarkDef;
! =========================================================
LEXICON MarkDef
<"+Indef":0 "@R.ART.INDEF@">		MarkN;
<"+Def":0 "@D.ART.INDEF@">		MarkN;
! =========================================================
LEXICON MarkN
<"+Noun":0 "@D.PN.T@" "@D.I.T@" "@D.C.T@">	MarkCaseNum;
<"+iNoun":0 "@R.I.T@">				MarkCaseNum;
<"+cNoun":0 "@R.C.T@">				MarkCaseNum;
<"+pNoun":0 "@R.PN.T@">				MarkCaseNum;
! =========================================================
LEXICON MarkCaseNum
!<"@D.PN.T@">                          	Ath;
< "+Ath":{ath} "@R.ART.INDEF@" "@R.NUM.SG@" "@D.PN.T@">    Wend;
<"+Gen":0 "@R.CASE.GEN@">             	Wend;
<"+Sg":0 "@R.NUM.SG@">                	Wend;
<"+Pl":0 "@R.NUM.PL@">                	Wend;
<"+Pl":0 "@R.PN.T@">                	Wend;
<"+Sg":{od} "@R.SUFF.OD@">	      	Wend;
<"+Sg":{og} "@R.SUFF.OG@"> 	        Wend;
<"+Sg":{ig} "@R.SUFF.IG@">		Wend;
! =========================================================
LEXICON Wend
                      #;
! =========================================================
END